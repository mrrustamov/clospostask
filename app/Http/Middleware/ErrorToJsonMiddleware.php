<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ErrorToJsonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle(Request $request, Closure $next)
    {
        $response=$next($request);
        if ($response->getStatusCode()!=200){
            return response()->json([
                'message'   => $response->exception->getMessage(),
                'http_code' => $response->getStatusCode()
            ]);
        }
        return $response;
    }
}
