<?php

namespace App\Exceptions;


use BadMethodCallException;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $message=null;

    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e){
           return response()->json([
               'message'=>$this->message,
               'exception'=>$e->getMessage(),
               'http_code'=>$e->getCode()
           ]);
        });

    }
    public function report(Throwable $exception)
    {


        $exception instanceof BadMethodCallException ? $this->message='Method Not Found' : null;
        $exception instanceof BindingResolutionException ? $this->message='Binding Resolution Not Exception' : null;
        $exception instanceof \ParseError ? $this->message='Parse Error  ' : null;

/*        Bütün errorları yazmaqa ehtiyac duymadım get_class($exception) functionu ilə exceptionun hansı instansdan olduğunu
          müəyyən edib message təyin etmək olar.*/



    }
}
